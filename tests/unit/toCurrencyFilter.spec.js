import { toCurrency } from '../../src/filters/toCurrency'

describe('host', () => {
    it('returns undefined if value is undefined', () => {
        expect(toCurrency(undefined)).toBeUndefined()
    })

    it('returns string if value is string', () => {
        expect(toCurrency("Test string")).toBe("Test string")
    })

    it('returns number in euro currency format', () => {
        expect(toCurrency(1234)).toBe("€1,234")
    })
})
