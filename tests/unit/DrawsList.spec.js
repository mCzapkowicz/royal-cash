import { mount, shallowMount } from '@vue/test-utils'
import DrawsList from '../../src/components/DrawsList'

describe('Section.vue', () => {
    it('Display no draws found message if draws property is an empty array', () => {
        const wrapper = mount(DrawsList, {
            propsData: {
                draws: []
            }
        })
        expect(wrapper.text()).toBe('No draws found')
    })
    it('renders a Section component if draws property has at least one item', () => {
        const wrapper = shallowMount(DrawsList, {
            propsData: {
                draws: [{}]
            }
        })
        expect(wrapper.findComponent({ name: "Section" }).exists()).toBe(true)
    })
})
