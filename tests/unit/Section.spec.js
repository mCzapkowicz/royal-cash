import { mount } from '@vue/test-utils'
import Section from '../../src/components/Section'

describe('Section.vue', () => {
    it('Title slot is rendered properly withing .section__title', () => {
        const wrapper = mount(Section, {
            slots: {
                title: '<div>This is test title</div>'
            }
        })
        const title = wrapper.find('.section__title')
        expect(title.text().trim()).toBe('This is test title')
    })
})
