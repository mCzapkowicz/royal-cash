import { createLocalVue, mount } from '@vue/test-utils'
import DrawDescription from '../../src/components/Draw/DrawDescription'
import { toCurrency } from '../../src/filters/toCurrency'

const localVue = createLocalVue()
localVue.filter('toCurrency', toCurrency)

describe('DrawDescription.vue', () => {
    it('Emits detailsToggle event on button click', () => {
        const wrapper = mount(DrawDescription, {
            propsData: {
                draw: {}
            }
        })
        wrapper.find('button').trigger('click')
        expect(Object.keys(wrapper.emitted())).toContain('detailsToggle')
    })
    it('Displays correctly draw date', () => {
        const wrapper = mount(DrawDescription, {
            propsData: {
                draw: {
                    date: 'Sep 25 2020'
                }
            }
        })
        const title = wrapper.find('.draw-description__header__date')
        expect(title.text().trim()).toBe('Sep 25 2020')
    })
})
