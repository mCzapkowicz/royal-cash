import { createLocalVue, mount } from '@vue/test-utils'
import Draw from '../../src/components/Draw/Draw'
import DrawDetails from '../../src/components/Draw/DrawDetails'
import DrawDescription from '../../src/components/Draw/DrawDescription'
import { toCurrency } from '../../src/filters/toCurrency'

const localVue = createLocalVue()
localVue.filter('toCurrency', toCurrency)

describe('Draw.vue', () => {
    const wrapper = mount(Draw, {
        propsData: {
            draw: {}
        }
    })
    it('DrawDetails component is not rendered on initial render', () => {
        expect(wrapper.findComponent(DrawDetails).exists()).toBeFalsy()
    })
    it('Toggles DrawDetails component visibility when DrawDescription component emits detailsToggle event', async () => {
        wrapper.findComponent(DrawDescription).vm.$emit('detailsToggle')
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(DrawDetails).exists()).toBeTruthy()
        wrapper.findComponent(DrawDescription).vm.$emit('detailsToggle')
        await wrapper.vm.$nextTick()
        expect(wrapper.findComponent(DrawDetails).exists()).toBeFalsy()
    })
})
