module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  watchPathIgnorePatterns: [
    "/node_modules/"
  ]
}
