module.exports = {
  chainWebpack: config => {
    config.module.rules.delete('eslint')
  },
  transpileDependencies: [
    /\bvue-awesome\b/
  ],
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "@/assets/styles/_variables.scss";'
      }
    }
  }
}
