export default {
    1: {
        correct: 5,
        euro: 2
    },
    2: {
        correct: 5,
        euro: 1
    },
    3: {
        correct: 5
    },
    4: {
        correct: 4,
        euro: 2
    },
    5: {
        correct: 4,
        euro: 1
    },
    6: {
        correct: 4
    },
    7: {
        correct: 3,
        euro: 2
    },
    8: {
        correct: 2,
        euro: 1
    },
    9: {
        correct: 3,
        euro: 1
    },
    10: {
        correct: 3
    },
    11: {
        correct: 1,
        euro: 2
    },
    12: {
        correct: 2,
        euro: 1
    }
}
