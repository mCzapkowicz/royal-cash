import Vue from 'vue'

export const toCurrency = function (value) {
    if (isNaN(parseInt(value, 10))) {
        return value
    }
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'EUR',
        minimumFractionDigits: 0
    })
    return formatter.format(value)
}

Vue.filter('toCurrency', toCurrency)
