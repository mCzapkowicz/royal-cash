import Vue from 'vue';
import App from './App.vue'
import router from './router'
import {createProvider} from './vue-apollo'
import './assets/styles/reset.css';
import './assets/styles/_common.scss';
import './filters/toCurrency';

new Vue({
    router,
    apolloProvider: createProvider(),
    render: h => h(App)
}).$mount('#app');
